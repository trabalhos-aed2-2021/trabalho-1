import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Main {
  public static void main(String[] args) {

    // salva o caminho dos aquivos de entrada e saida. Obs.: abra o VS code na pasta
    // "Trabalho-1"
    String entrada = System.getProperty("user.dir") + "\\codigo\\src\\entrada.txt";
    String saida = System.getProperty("user.dir") + "\\codigo\\src\\saida.txt";

    try {
      // cria variaveis auxiliares para leitura e manipulação dos dados de entrada
      FileReader lerAqruivo = new FileReader(entrada);
      BufferedReader buffer = new BufferedReader(lerAqruivo);
      String linha = buffer.readLine();
      int i = 1;

      // cria um objeto da TAD de blocos
      TBlocos blocos = null;

      // lê as linhas do arquivo de entrada enquanto não chegar na linha "quit"
      while (!linha.equals("quit")) {
        String[] metodos = linha.split(" "); // separa as palavras da linha atual
        if (i == 1) { // caso for a 1ª linha entra no if e identifica a qtd de blocos
          Integer.parseInt(metodos[0]);
          blocos = new TBlocos(Integer.parseInt(metodos[0]));
        } else if (!metodos[0].equals("quit")) { // entra na condição caso for um comando diferente de "quit"

          // identifica quais blocos iremos manipular no comando da linha atual
          int a = Integer.parseInt(metodos[1]);
          int b = Integer.parseInt(metodos[3]);

          // condicional para verificar se os blocos são iguais ou estão na mesma
          // linha, caso estejam o programa ignora o comando
          if (a != b && blocos.getPosition(a) != blocos.getPosition(b)) {

            // verifica se o comando se trata de um "move" ou "pile" e após isso
            // verifica se é do tipo "onto" ou "over"
            switch (metodos[0]) {
            case "move":
              if (metodos[2].equals("onto")) {
                blocos.moveOnto(a, b);
              } else {
                blocos.moveOver(a, b);
              }
              break;
            case "pile":
              if (metodos[2].equals("onto")) {
                blocos.pileOnto(a, b);
              } else {
                blocos.pileOver(a, b);
              }
              break;
            }
          }
        }
        System.out.println(blocos);
        linha = buffer.readLine();
        i++;
      }

      // cria as variaveis para escrever no arquivo de saida
      FileWriter escrever = new FileWriter(saida, false);
      BufferedWriter bufferWr = new BufferedWriter(escrever);
      bufferWr.write(blocos.toString());

      System.out.println(blocos);
      bufferWr.close();
      buffer.close();

    } catch (FileNotFoundException e) {
      System.out.println("o arquivo não existe.");
    } catch (IOException e) {
      System.out.println("Erro na leitura do arquivo.");
    }
  }
}
