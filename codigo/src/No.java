public class No {

    private int info; // atributo que armazena o número do bloco
    private No proximo; // atributo que armazena o próximo bloco da lista

    // gets e sets
    public int getInfo() {
        return this.info;
    }

    public void setInfo(int info) {
        this.info = info;
    }

    public No getProximo() {
        return this.proximo;
    }

    public void setProximo(No proximo) {
        this.proximo = proximo;
    }
}
