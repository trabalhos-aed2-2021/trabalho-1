public class ListaEncadeada {
    private No inicio = null; // atributo que armazena o primeiro bloco da lista
    private int tamanho = 0; // atributo que armazena o tamanho da lista

    /// gets e sets
    public No getInicio() {
        return this.inicio;
    }

    public void setInicio(No inicio) {
        this.inicio = inicio;
    }

    public int getTamanho() {
        return this.tamanho;
    }

    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }

    // comandos auxiliares

    // insere um item no inicio de uma lista
    public void inserirInicio(int info) {
        No no = new No();
        no.setInfo(info);
        no.setProximo(getInicio());
        setInicio(no);
        setTamanho(getTamanho() + 1);
    }

    // retira um item do inicio de uma lista
    public int retirarInicio() {
        if (getInicio() == null) {
            return 0;
        }
        int info = inicio.getInfo();
        setInicio(getInicio().getProximo());
        setTamanho(getTamanho() - 1);
        return info;
    }

    // insere um item no fim de uma lista
    public void inserirFim(int info) {
        No no = new No();
        no.setInfo(info);
        if (getInicio() == null) {
            no.setProximo(null);
            setInicio(no);
        } else {
            No local = getInicio();
            while (local.getProximo() != null) {
                local = local.getProximo();
            }
            local.setProximo(no);
            no.setProximo(null);
        }
        setTamanho(getTamanho() + 1);
    }

    // retira um item do fim de uma lista
    public int retirarFim() {
        if (getInicio() == null) {
            return 0;
        }
        No local = getInicio();
        while (local.getProximo() != null) {
            No aux = local;
            local = local.getProximo();
            if (local.getProximo() == null) {
                aux.setProximo(null);
                setTamanho(getTamanho() - 1);
                return local.getInfo();
            }
        }
        setInicio(null);
        setTamanho(getTamanho() - 1);
        return local.getInfo();
    }

    // insere um item na posição informada pelo usuário
    public void inserirIndice(int indice, int info) {
        if (indice <= 0) {
            inserirInicio(info);
        } else if (indice >= tamanho) {
            inserirFim(info);
        } else {
            No local = getInicio();
            for (int i = 0; i < indice - 1; i++) {
                local = local.getProximo();
            }
            No no = new No();
            no.setInfo(info);
            no.setProximo(local.getProximo());
            local.setProximo(no);
            setTamanho(getTamanho() + 1);
        }
    }

    // retira um item da posição informada pelo usuário
    public int retirarIndice(int indice) {
        if (indice < 0 || indice >= getTamanho() || getInicio() == null) {
            return 0;
        } else if (indice == 0) {
            return retirarInicio();
        } else if (indice == getTamanho() - 1) {
            return retirarFim();
        }
        No local = getInicio();
        for (int i = 0; i < indice - 1; i++) {
            local = local.getProximo();
        }
        int info = local.getProximo().getInfo();
        local.setProximo(local.getProximo().getProximo());
        setTamanho(getTamanho() - 1);
        return info;
    }

    // verifica se um item existe na lista atual
    public boolean valorExiste(int valor) {
        boolean saida = false;
        No local = getInicio();
        while (local != null) {
            if (local.getInfo() == valor) {
                saida = true;
            }
            local = local.getProximo();
        }

        return saida;
    }

    // obtem a posição de um item na lista atual caso ele exista, se não retorna -1
    public int obterIndice(int valor) {
        int index = -1;
        int i = 0;

        if (valorExiste(valor)) {
            No local = getInicio();
            while (local != null) {
                if (local.getInfo() == valor) {
                    index = i;
                }
                local = local.getProximo();
                i++;
            }
        }

        return index;
    }

    // obtem o valor presente no indice informado
    public int obterValorNoIndice(int indice) {
        int valor = 0;
        int i = 0;

        No local = getInicio();
        while (local != null) {
            if (i == indice) {
                valor = local.getInfo();
            }
            local = local.getProximo();
            i++;
        }

        return valor;
    }

    // retorna a lista em forma de string
    public String toString() {
        String str = "";
        No local = getInicio();
        while (local != null) {
            str += local.getInfo() + " ";
            local = local.getProximo();
        }
        return str;
    }
}