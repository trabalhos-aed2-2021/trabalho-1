public class TBlocos {
  // atributos da classe
  private int tamanho; // tamanho do mundo de blocos
  private ListaEncadeada[] listas; // cada lista encadeada é um monte

  // construtor que recebe o tamanho do mundo de blocos
  public TBlocos(int _tamanho) {
    setTamanho(_tamanho);
    inicializaListas();
  }

  // gets e sets
  private int getTamanho() {
    return this.tamanho;
  }

  private void setTamanho(int tamanho) {
    this.tamanho = tamanho;
  }

  public ListaEncadeada[] getListas() {
    return this.listas;
  }

  // *****Metodos auxiliares*****

  // inicializa o mundo de blocos de acordo com o tamanho passado no construtor
  private void inicializaListas() {
    this.listas = new ListaEncadeada[getTamanho()];
    for (int i = 0; i < getTamanho(); i++) {
      this.listas[i] = new ListaEncadeada();
      this.listas[i].inserirInicio(i);
    }
  }

  // metodo para imprimir um TBloco em forma de string
  public String toString() {
    String saida = "";
    int i = 0;
    for (ListaEncadeada lista : getListas()) {
      saida += i + ": " + lista.toString() + "\n";
      i++;
    }

    return saida;
  }

  // retorna em qual monte um determinado bloco se encontra
  public int getPosition(int number) {
    int index = 0;
    int i = 0;
    for (ListaEncadeada lista : getListas()) {
      if (lista.valorExiste(number)) {
        index = i;
      }
      i++;
    }

    return index;
  }

  // metodo que recebe uma lista(monte) e um bloco e retorna todos os blocos que
  // estão em cima do bloco informado para suas posições originais
  private void returnOriginalPosition(ListaEncadeada lista, int number) {
    while ((lista.getTamanho() - 1) > (lista.obterIndice(number))) { // executa enquanto houver blocos em cima do bloco
                                                                     // informado
      int aux = lista.obterValorNoIndice((lista.getTamanho() - 1)); // obtem o bloco que esta mais acima do bloco
                                                                    // informado, caso haja
      lista.retirarFim(); // retira ele da lista passada como parametro
      this.listas[aux].inserirInicio(aux); // insere na sua posição original
    }
  }

  // metodo que move os blocos que estão sobre o bloco passado como paramentro
  // e ele mesmo para cima da da segunda lista passada como parametro
  private void movePile(ListaEncadeada _listaA, ListaEncadeada _listaB, int _a) {
    int idxA = _listaA.obterIndice(_a); // armazena o indice do valor A
    while ((_listaA.getTamanho() - 1) >= idxA) {
      _listaB.inserirFim(_listaA.obterValorNoIndice(idxA));
      _listaA.retirarIndice(idxA);
    }
  }

  // *****Comandos para manipulação do Mundo dos blocos*****

  // move A onto B
  public void moveOnto(int _a, int _b) {
    // obtem as listas(montes) em que A e B estão
    ListaEncadeada listaA = getListas()[getPosition(_a)];
    ListaEncadeada listaB = getListas()[getPosition(_b)];

    // retorna os blocos que estão em cima de A e B para suas posições originais
    returnOriginalPosition(listaA, _a);
    returnOriginalPosition(listaB, _b);

    // retira o bloco A do monte onde ele esta
    listaA.retirarIndice(listaA.obterIndice(_a));
    // insere o bloco A logo acima do bloco B
    listaB.inserirFim(_a);

    // atualiza o monte onde B esta com o novo monte
    this.listas[getPosition(_b)] = listaB;
  }

  // move A over B
  public void moveOver(int _a, int _b) {
    // executa as mesmas coisas do metodo anterior, porem não retorna os blocos
    // que estão em cima de B para suas posições

    ListaEncadeada listaA = getListas()[getPosition(_a)];
    ListaEncadeada listaB = getListas()[getPosition(_b)];

    returnOriginalPosition(listaA, _a);

    listaA.retirarIndice(listaA.obterIndice(_a));
    listaB.inserirFim(_a);

    this.listas[getPosition(_b)] = listaB;
  }

  public void pileOnto(int _a, int _b) {
    // obtem as listas(montes) em que A e B estão
    ListaEncadeada listaA = getListas()[getPosition(_a)];
    ListaEncadeada listaB = getListas()[getPosition(_b)];

    // retorna os blocos que estão em cima de B para suas posições originais
    returnOriginalPosition(listaB, _b);

    // pega o bloco A e todos que estavam sobre ele, e coloca em cima do bloco B
    movePile(listaA, listaB, _a);

    // atualiza o monte de B com os novos valores
    this.listas[getPosition(_b)] = listaB;
  }

  public void pileOver(int _a, int _b) {
    // executa as mesmas coisas do metodo anterior, porem não retorna os blocos
    // que estão em cima de B para suas posições
    ListaEncadeada listaA = getListas()[getPosition(_a)];
    ListaEncadeada listaB = getListas()[getPosition(_b)];

    movePile(listaA, listaB, _a);

    this.listas[getPosition(_b)] = listaB;
  }
}